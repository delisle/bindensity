
bindensity documentation
========================

The bindensity package provides a suite of functions to manipulate
bins densities and covariance matrices.

Installation
------------

Using conda
~~~~~~~~~~~

The bindensity package can be installed using conda with the following command:

``conda install -c conda-forge bindensity``

Using pip
~~~~~~~~~

It can also be installed using pip with:

``pip install bindensity``

API Reference
-------------

.. autosummary::
   :toctree: _autosummary
   :template: autosummary/custom_module.rst
   :recursive:

   bindensity
