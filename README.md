bindensity
==========

Read the documentation at https://obswww.unige.ch/~delisle/bindensity/doc/.

Contribute
----------

Everyone is welcome to open issues and/or contribute code via pull-requests.
A SWITCH edu-ID account is necessary to sign in to https://gitlab.unige.ch.
If you don't have an account, you can easily create one at https://eduid.ch.
Then you can sign in to https://gitlab.unige.ch by selecting "SWITCH edu-ID" as your organisation.
